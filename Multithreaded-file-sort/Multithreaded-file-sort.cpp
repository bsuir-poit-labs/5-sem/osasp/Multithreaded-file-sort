﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

#include "TaskQueue.h"
#include "TaskHandler.h"

using namespace std;

#define FILE_PATH "text.txt"
#define THREADS_COUNT 4

vector<string> MergeDataParts(vector<vector<string>*> dataParts);
TaskQueue* FormTaskQueue(vector<string> data, int threadsCount);
vector<string> GetFileContent(string filename);
void PrintData(vector<string> data);

vector<vector<string>*> dataParts;

int main()
{
	vector<string> data = GetFileContent(FILE_PATH);
	PrintData(data);
	cout << endl << endl;


	int threadsCount;
	if (THREADS_COUNT > data.size()) {
		threadsCount = data.size();
	}
	else {
		threadsCount = THREADS_COUNT;
	}

	TaskQueue* taskQueue = FormTaskQueue(data, threadsCount);
	TaskHandler* taskHandler = new TaskHandler(taskQueue);
	taskHandler->sortTasks(threadsCount);

	vector<string> sortedData = MergeDataParts(dataParts);
	PrintData(sortedData);
}

vector<string> MergeDataParts(vector<vector<string>*> dataParts) {
	vector<string> sortedData;
	vector<string> lines;
	for (int i = 0; i < dataParts.size(); i++)
	{
		lines.push_back(dataParts[i]->front());
		dataParts[i]->erase(dataParts[i]->begin());
	}

	while (!dataParts.empty()) {
		string minLine = lines[0];
		int minIndex = 0;

		for (int i = 1; i < lines.size(); i++) {
			if (strcmp(lines[i].c_str(), minLine.c_str()) < 0) {
				minLine = lines[i];
				minIndex = i;
			}
		}

		sortedData.push_back(minLine);

		if (dataParts[minIndex]->empty()) {
			dataParts.erase(dataParts.begin() + minIndex);
			lines.erase(lines.begin() + minIndex);
		}
		else {
			lines[minIndex] = dataParts[minIndex]->front();
			dataParts[minIndex]->erase(dataParts[minIndex]->begin());
		}
	}

	return sortedData;
}

TaskQueue* FormTaskQueue(vector<string> data, int threadsCount) {
	int part = data.size() / threadsCount;
	TaskQueue* taskQueue = new TaskQueue();

	for (int i = 0; i < threadsCount; i++) {
		vector<string>* dataPart = new vector<string>();
		for (int j = 0; j < part; j++) {
			string str = data[i * part + j];
			dataPart->push_back(str);
		}
		dataParts.push_back(dataPart);
		taskQueue->setTask([dataPart]() { sort(dataPart->begin(), dataPart->end()); });
	}
	return taskQueue;
}

vector<string> GetFileContent(string filename) {
	vector<string> data;

	ifstream file(filename);
	if (!file.is_open()) {
		cout << "Can't Open file!" << filename << endl;
		return vector<string>();
	}

	string line;
	while (getline(file, line)) {
		if (line.length() != 0) {
			data.push_back(line);
		}
	}

	return data;
}

void PrintData(vector<string> data) {
	for (string str : data) {
		cout << str << endl;
	}
}
