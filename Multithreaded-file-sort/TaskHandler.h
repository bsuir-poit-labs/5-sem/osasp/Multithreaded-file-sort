#pragma once
#include "TaskQueue.h"
#include <thread>
#include <string>

class TaskHandler
{
private:
	TaskQueue* taskQueue;

public:
	TaskHandler(TaskQueue* taskQueue);
	void sortTasks(int threadCounts);
};

