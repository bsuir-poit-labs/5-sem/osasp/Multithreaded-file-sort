#pragma once
#include <functional>
#include <mutex>
#include <queue>
#include <string>

using namespace std;

typedef function<void()> Task;

class TaskQueue
{
private:
	mutex mutex;
	queue<Task>* tasks = new queue<Task>;

public:
	TaskQueue();
	Task getTask();
	void setTask(Task task);
};

