#include "TaskQueue.h"

TaskQueue::TaskQueue() {};

Task TaskQueue::getTask()
{
	Task task;

	mutex.lock();
	if (tasks->empty()) {
		task = NULL;
	}
	else {
		task = tasks->front();
		tasks->pop();
	}
	mutex.unlock();

	return task;
}

void TaskQueue::setTask(Task task)
{
	mutex.lock();
	tasks->push(task);
	mutex.unlock();
}
